<?php

use MediaWiki\MediaWikiServices;

class ConfigHacksHooks {

	private const NONSA_HIDE_BLUEBAR = 'nonsa-show-bluebar';

	/**
	 * Override Echo config.
	 *
	 * @param $notifs
	 * @param $cats
	 * @param $icons
	 */
	public static function onBeforeCreateEchoEvent( &$notifs, &$cats, &$icons ) {
		unset( $notifs["thank-you-edit"] );
		unset( $cats['thank-you-edit'] );
		unset( $cats['emailuser'] );
		unset( $cats['edit-user-talk']['no-dismiss'] );

		foreach ( array_keys( $notifs ) as $k ) {
			$notifs[$k]['section'] = 'alert';
		}
	}

	public static function onGetPreferences( User $user, array &$preferences ) {
		$preferences[self::NONSA_HIDE_BLUEBAR] = [
			'type' => 'toggle',
			'section' => 'echo/echotalkpage',
			'label-message' => 'nonsa-pref-show-bluebar',
			'help-message' => 'nonsa-pref-show-bluebar-help',
		];
	}

	/**
	 * Now THIS is a masterpiece of a hook – IT DOES NOT HAVE ANY PARAMETERS!
	 * RequestContext::getMain() has never been classier!
	 *
	 * @return bool true to hide the notification bar, false otherwise
	 */
	public static function onEchoCanAbortNewMessagesAlert() : bool {
		$user = RequestContext::getMain()->getUser();
		$options = MediaWikiServices::getInstance()->getUserOptionsLookup();

		return !$options->getBoolOption( $user, self::NONSA_HIDE_BLUEBAR );
	}

	/**
	 * Modify Timeless' definition.
	 */
	public static function onRegistration() : void {
		global $wgValidSkinNames,
			   $wgContentNamespaces,
			   $wgNamespacesToBeSearchedDefault;

		$wgValidSkinNames['timeless']['args'][0]['template'] = 'TimelessNonsaTemplate';

		// This is due to SMW being utterly dumb.
		// Just override it.
		$wgContentNamespaces = [ 0, 100, 102, 104, 106, 108, 114 ];
		$wgNamespacesToBeSearchedDefault = [];
		foreach ( $wgContentNamespaces as $ns ) {
			$wgNamespacesToBeSearchedDefault[$ns] = true;
		}
	}

	public static function onSvetovidAddLinks(
		Title $targetTitle,
		WikiPage $page,
		array $texts,
		string &$text,
		int &$changes
	) : bool {
		if ( $page->getTitle()->getNamespace() !== NS_FILE ) {
			return true;
		}

		if ( preg_match(
			'/\|\s*[Cc]aption\s*=\s*([^\[|]+(\[\[[^|\]]+\|?.*?]])?)+/',
			$text,
			$matches
		) ) {
			$caption = $matches[0];
			$changes = SvetovidTextProcessing::addLinks( $caption, $targetTitle, $texts );
			$text = str_replace( $matches[0], $caption, $text );
		}

		return false;
	}

	public static function onParserFirstCallInit( Parser $parser ) {
		$parser->setHook( 'discord', [ self::class, 'renderTagDiscord' ] );
	}

	public static function renderTagDiscord() : string {
		return Html::element( 'iframe', [
			'src' => 'https://discordapp.com/widget?id=835598579404898344&theme=dark',
			'width' => 300,
			'height' => 450,
			'allowtransparency' => 'true',
			'frameborder' => 0,
			'sandbox' => 'allow-popups allow-popups-to-escape-sandbox allow-same-origin allow-scripts'
		] );
	}

	/**
	 * @param OutputPage $out
	 * @param Skin $skin
	 */
	public static function onBeforePageDisplay( OutputPage $out, Skin $skin ) {
		$out->addHTML( Html::rawElement(
			'div',
			[
				'id' => 'railmodule-preload',
				'style' => 'display: none'
			],
			$out->msg( 'railModule' )->parse()
		) );
	}

	public static function onResourceLoaderRegisterModules( ResourceLoader $resourceLoader ) {
		// unregister this crappy module, it calls an XHR on every view (!!!)
		// https://www.semantic-mediawiki.org/wiki/Help:Entity_examinations
		$resourceLoader->register( 'smw.entityexaminer', [] );
	}
}
